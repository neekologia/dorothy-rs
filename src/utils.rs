use std::error::Error;
use std::fs::File;
use std::io::BufReader;

use rand::seq::SliceRandom;
use serde_json::from_str;
use teloxide::{
	payloads::{SendAnimationSetters, SendPhotoSetters, SendVideoSetters},
	requests::Requester,
	types::{ChatId, InlineKeyboardButton, InlineKeyboardMarkup, InputFile, ParseMode},
	Bot,
};
use teloxide::RequestError;
use teloxide::RequestError::Api;
use teloxide::ApiError::{WrongFileIdOrUrl, FailedToGetUrlContent};
use teloxide_core::adaptors::Throttle;

pub mod db;
use db::{Booru, BooruChat, BooruPost, DanbooruPost, GelbooruSource, User, SendFeedPost};

pub fn make_send_keyboard(rating: &str) -> InlineKeyboardMarkup {
	let mut row: Vec<InlineKeyboardButton> = vec![];

	row.push(InlineKeyboardButton::callback("send", rating));
	row.push(InlineKeyboardButton::callback("delete", "delete"));
	InlineKeyboardMarkup::new(vec![row])
}

pub fn read_users() -> Result<Vec<User>, Box<dyn Error>> {
	let path = "db/users";

	// Open the file in read-only mode with buffer.
	let file = File::open(path)?;
	let reader = BufReader::new(file);

	let u = serde_json::from_reader(reader)?;

	Ok(u)
}

pub fn read_booru() -> Result<Vec<Booru>, Box<dyn Error>> {
	let path = "db/booru";

	// Open the file in read-only mode with buffer.
	let file = File::open(path)?;
	let reader = BufReader::new(file);

	let u = serde_json::from_reader(reader)?;

	Ok(u)
}

pub async fn get_booru_posts(url: &str) -> Result<Option<Vec<BooruPost>>, Box<dyn Error>> {
	let req_client = reqwest::Client::builder()
		.user_agent("dorothy-rs")
		.build()?;
	let booru_response = req_client.get(url.clone()).send().await?;
	let response_string = booru_response.text().await?;

	if response_string == "" {
		return Ok(None);
	}

	match from_str::<GelbooruSource>(&response_string) {
		Ok(gelbooru) => {
			if let Some(posts) = gelbooru.post {
				return Ok(Some(posts));
			}
		}
		Err(_) => (),
	}
	match from_str::<Vec<BooruPost>>(&response_string) {
		Ok(posts) => {
			return Ok(Some(posts));
		}
		Err(_) => (),
	}
	match from_str::<DanbooruPost>(&response_string) {
		Ok(danbooru_post) => {
			let Some(original) = danbooru_post.media_asset.variants
				.iter().find(|v| v.r#type == "original") else {
				return Ok(None);
			};
			let file_url = original.url.clone();
			let sample_url = if let Some(sample) = danbooru_post
				.media_asset
				.variants
				.iter()
				.find(|v| v.r#type == "sample")
			{
				sample.url.clone()
			} else {
				String::new()
			};

			let booru_post = BooruPost {
				id: danbooru_post.id,
				file_url,
				sample_url,
				rating: danbooru_post.rating,
				tags: danbooru_post.tag_string,
				md5: Some(danbooru_post.md5),
				hash: None,
			};
			return Ok(Some(vec![booru_post]));
		}
		Err(_) => return Ok(None),
	}
}

async fn get_method_error(
	bot: &Throttle<Bot>,
	send_feed_post: &SendFeedPost
) -> Option<RequestError> {
	match send_feed_post.method.as_str() {
		"photo" => {
			let result = bot
				.send_photo(send_feed_post.chat_id, send_feed_post.input_file.clone())
				.caption(send_feed_post.caption.clone())
				.reply_markup(send_feed_post.reply_markup.clone())
				.parse_mode(send_feed_post.parse_mode)
				.await;
			if let Err(err) = result {
				return Some(err);
			} else {
				return None;
			};
		}
		"video" => {
			let result = bot
				.send_video(send_feed_post.chat_id, send_feed_post.input_file.clone())
				.caption(send_feed_post.caption.clone())
				.reply_markup(send_feed_post.reply_markup.clone())
				.parse_mode(send_feed_post.parse_mode)
				.await;
			if let Err(err) = result {
				return Some(err);
			} else {
				return None;
			};
		}
		"animation" => {
			let result = bot
				.send_animation(send_feed_post.chat_id, send_feed_post.input_file.clone())
				.caption(send_feed_post.caption.clone())
				.reply_markup(send_feed_post.reply_markup.clone())
				.parse_mode(send_feed_post.parse_mode)
				.await;
			if let Err(err) = result {
				return Some(err);
			} else {
				return None;
			};
		}
		_ => return None,
	}
}

pub async fn send_feed_post(
	bot: &Throttle<Bot>,
	chat_id: ChatId,
	booru_post: BooruPost,
	caption: &str,
	button_data: &str,
) {
	let Ok(file_url) = url::Url::parse(&booru_post.file_url) else {
		return;
	};
	let input_file = InputFile::url(file_url.clone());

	let Some(extension) = std::path::Path::new(file_url.path()).extension() else {
		return;
	};
	let Some(extension_str) = extension.to_str() else {
		return;
	};
	let method = match extension_str {
		"jpg" | "jpeg" | "png" => "photo".to_string(),
		"mp4" => "video".to_string(),
		"gif" => "animation".to_string(),
		_ => return,
	};
	let parse_mode = ParseMode::Html;
	let reply_markup = make_send_keyboard(&button_data);
	let mut send_feed_post = SendFeedPost {
		method,
		input_file,
		chat_id,
		caption: caption.to_string(),
		parse_mode,
		reply_markup,
	};

	let err = get_method_error(&bot, &send_feed_post).await;
	let try_sample_url = match err {
		Some(Api(WrongFileIdOrUrl)) | Some(Api(FailedToGetUrlContent)) => {
			true
		}
		_ => false,
	};
	if try_sample_url {
		let Ok(sample_url) = url::Url::parse(&booru_post.sample_url) else {
			return;
		};
		send_feed_post.input_file = InputFile::url(sample_url.clone());
		let Some(err) = get_method_error(&bot, &send_feed_post).await else {
			return;
		};
		println!("{:?}", err);
	}
}

pub async fn get_booru_post_tags(
	booru_post: &BooruPost,
	booru_chat: Option<&BooruChat>,
) -> Vec<String> {
	let mut hashtags: Vec<String> = vec![];
	if let Some(booru_chat) = booru_chat {
		for tag in &booru_chat.tags {
			let tag_match = booru_post
				.tags
				.split(" ")
				.filter(|&booru_tag| booru_tag == tag)
				.collect::<Vec<&str>>()
				.pop();
			match tag_match {
				Some(tag) => {
					hashtags.push("#".to_owned() + tag);
				}
				None => (),
			}
		}
	}
	let mut booru_tags_split: Vec<&str> = booru_post.tags.split(" ").collect();
	let mut booru_tags: Vec<String> = vec![];
	for booru_tag in booru_tags_split.drain(..) {
		booru_tags.push("#".to_owned() + &booru_tag);
	}
	let mut rng = rand::thread_rng();
	booru_tags.shuffle(&mut rng);
	for booru_tag in booru_tags.iter() {
		if !hashtags.contains(&booru_tag.to_string()) {
			hashtags.push(booru_tag.to_string());
		}
		if hashtags.len() == 3 {
			break;
		}
	}
	return hashtags;
}
