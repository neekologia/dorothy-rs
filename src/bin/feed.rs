use dorothy_utils::{
	read_booru,
	utils::db::{Booru, BooruChat},
	utils::{get_booru_post_tags, get_booru_posts, send_feed_post},
};
use std::io::{BufRead, Write};
use teloxide::net;
use teloxide::prelude::RequesterExt;
use teloxide::types::ChatId;
use teloxide::Bot;
use teloxide_core::adaptors::throttle::Limits;
use teloxide_core::adaptors::Throttle;
use tokio::time::Duration;

async fn has_md5(feed: &str, md5: &str) -> bool {
	let path = format!("db/{feed}");
	std::fs::create_dir_all(path.clone()).unwrap();
	let md5_file = std::fs::File::options()
		.create(true)
		.write(true)
		.read(true)
		.open(&format!("{path}/md5"))
		.unwrap();
	let md5_reader = std::io::BufReader::new(md5_file);
	for line in md5_reader.lines() {
		if line.unwrap().trim() == md5 {
			return true;
		}
	}
	false
}

async fn write_md5(feed: &str, md5: &str) {
	let path = format!("db/{feed}");
	let mut md5_file = std::fs::File::options()
		.append(true)
		.open(&format!("{path}/md5"))
		.unwrap();
	writeln!(&mut md5_file, "{}", md5).unwrap();
}

async fn read_lastid(feed: &str, tag: &str, rating: &str, website: &str) -> u64 {
	let path = format!("db/{feed}/{tag}/{rating}/{website}/");
	std::fs::create_dir_all(path.clone()).unwrap();
	let lastid_file = std::fs::File::options()
		.create(true)
		.write(true)
		.read(true)
		.open(&format!("{path}/lastid"))
		.unwrap();
	let mut lastid_reader = std::io::BufReader::new(lastid_file);
	let mut lastid_str = String::new();
	lastid_reader.read_line(&mut lastid_str).unwrap();
	lastid_str = lastid_str.trim().to_string();

	if lastid_str == "" {
		if website.contains("api") {
			return 8115133;
		} else {
			return 8685574;
		}
	} else {
		return lastid_str.parse::<u64>().unwrap();
	}
}

async fn write_lastid(feed: &str, tag: &str, rating: &str, website: &str, lastid: u64) {
	let path = format!("db/{feed}/{tag}/{rating}/{website}/");
	let mut lastid_file = std::fs::File::create(&format!("{path}/lastid")).unwrap();
	writeln!(&mut lastid_file, "{}", lastid).unwrap();
}

async fn send_posts_in_chat(
	bot: &Throttle<Bot>,
	base_req: &str,
	api_key: &str,
	booru: &Booru,
	chat: &BooruChat,
) {
	let amount = booru.amount;
	let lastid = read_lastid(
		&booru.name,
		&chat.tags.join("_"),
		&booru.rating,
		&booru.website,
	)
	.await;

	let chat_req = format!("{base_req}+{}", chat.tags.join("+"));
	let url = format!(
		"https://{}/{}&limit={amount}&tags={}+id:>{lastid}+sort:id:asc{api_key}",
		booru.website, "index.php?page=dapi&s=post&q=index&json=1", chat_req
	);

	let Some(booru_posts) = get_booru_posts(&url).await.unwrap() else {
		return;
	};
	let mut post_ids: Vec<u64> = vec![];
	for booru_post in booru_posts {
		let hash = match booru_post.clone().md5 {
			Some(md5) => md5,
			None => match booru_post.clone().hash {
				Some(md5) => md5,
				None => panic!("hash not found"),
			},
		};

		let mut hashtags = get_booru_post_tags(&booru_post, Some(chat)).await;
		if let Some(ref replace) = booru.replace {
			let mut hashtags_sub: Vec<String> = vec![];
			for (pattern, sub) in replace {
				for tag in hashtags.drain(..) {
					if tag.contains(*&pattern) {
						hashtags_sub.push(tag.replace(pattern, sub));
					} else {
						hashtags_sub.push(tag);
					}
				}
			}
			hashtags = hashtags_sub;
		}

		let source_website = match booru.website.strip_prefix("api.") {
			Some(source_website) => source_website,
			None => &booru.website,
		};
		let source = format!(
			"https://{}/index.php?page=post&s=view&id={}",
			source_website, booru_post.id
		);
		let caption = format!(
			"{} — <a href=\"{source}\">sauce</a>",
			hashtags.join(" ").replace(
				['\\', '!', '\'', ':', '{', '}', '+', '~', '(', ')', '.', ',', '/', '-'],
				""
			)
		);

		if !has_md5(&booru.name, &hash).await {
			send_feed_post(
				&bot,
				ChatId(chat.id),
				booru_post.clone(),
				&caption,
				&booru.button,
			)
			.await;
			write_md5(&booru.name, &hash).await;
		}

		post_ids.push(booru_post.id);
	}
	let max_id = post_ids.iter().fold(0, |acc, id| acc.max(*id));
	write_lastid(
		&booru.name,
		&chat.tags.join("_"),
		&booru.rating,
		&booru.website,
		max_id,
	)
	.await;
}

async fn send_posts_in_booru(bot: &Throttle<Bot>, booru: &Booru) {
	if !booru.active {
		return;
	}
	let mut base_req = format!("{}+", booru.rating);
	let api_key: String = if let Some(api_key) = &booru.api_key {
		api_key.to_string()
	} else {
		String::new()
	};
	base_req += &booru
		.blacklist
		.iter()
		.map(|e| "-".to_owned() + e)
		.collect::<Vec<String>>()
		.join("+");
	println!("start {}", booru.name);
	for chat in &booru.chats {
		send_posts_in_chat(&bot, &base_req, &api_key, &booru, &chat).await;
	}
	println!("end {}", booru.name);
}

async fn send_posts(bot: &Throttle<Bot>) {
	let booru_list = read_booru().unwrap();
	let to_join = booru_list.iter().map(|booru| {
		return send_posts_in_booru(&bot, &booru);
	});
	futures::future::join_all(to_join).await;
}

#[tokio::main]
async fn main() {
	let client = net::default_reqwest_settings()
		.timeout(Duration::from_secs(60))
		.build()
		.unwrap();
	let bot = Bot::from_env_with_client(client).throttle(Limits::default());
	loop {
		send_posts(&bot).await;
		std::thread::sleep(std::time::Duration::from_secs(3600));
	}
}
