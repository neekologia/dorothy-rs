use std::io::Write;
use std::{borrow::Cow, error::Error};
use teloxide::{
	prelude::*,
	types::{ChatId, InputFile, Me, MessageEntityKind, MessageId, ParseMode::Html},
	update_listeners::webhooks,
	utils::command::BotCommands,
};
use teloxide_core::adaptors::throttle::Limits;
use teloxide_core::adaptors::Throttle;
use url::Url;

use dorothy_utils::UserChat;
use dorothy_utils::{get_booru_post_tags, get_booru_posts, read_users, send_feed_post};

#[derive(BotCommands)]
#[command(
	rename_rule = "lowercase",
	description = "These commands are supported:"
)]
enum Command {
	#[command(description = "Display this text")]
	Help,
	#[command(description = "Ping")]
	Ping,
	Booru,
}

async fn message_handler(
	bot: Throttle<Bot>,
	msg: Message,
	me: Me,
) -> Result<(), Box<dyn Error + Send + Sync>> {
	if let Some(text) = msg.text() {
		match BotCommands::parse(text, me.username()) {
			Ok(Command::Help) => {
				// Just send the description of all commands.
				bot.send_message(msg.chat.id, Command::descriptions().to_string())
					.await?;
			}
			Ok(Command::Ping) => {
				bot.send_message(msg.chat.id, "pong".to_string()).await?;
			}
			Ok(Command::Booru) => {
				let Some(message_user) = msg.from() else {
					return Ok(())
				};
				let Some(message_arg) = text.split_whitespace().nth(1) else {
					return Ok(())
				};
				let Ok(message_url) = Url::parse(message_arg) else {
					return Ok(())
				};
				let Some(domain) = message_url.domain() else {
					return Ok(())
				};

				let post_id: String;
				let url_query: String;
				if domain == "danbooru.donmai.us" {
					match message_url.path_segments() {
						Some(segments) => {
							let Some(last_seg) = segments.last() else {
								return Ok(())
							};
							let Ok(last_seg) = last_seg.parse::<u64>() else {
								return Ok(())
							};
							post_id = last_seg.to_string();
						}
						None => return Ok(()),
					};
					url_query = format!("https://{}/posts/{post_id}.json", domain);
				} else {
					let Some(post_id_query) = message_url
						.query_pairs()
						.find(|q| q.0 == Cow::Borrowed("id")) else {
						return Ok(())
					};
					post_id = format!("{}={}", post_id_query.0, post_id_query.1);
					url_query = format!(
						"https://{}/index.php?page=dapi&s=post&q=index&json=1&{}",
						domain, post_id
					);
				}

				let booru_users = read_users().unwrap();
				let Some(_booru_user) = booru_users
					.iter()
					.find(|&user| user.id == message_user.id.0) else {
					return Ok(())
				};
				let booru_posts = match get_booru_posts(&url_query).await {
					Ok(booru_posts) => booru_posts,
					Err(err) => {
						println!("{:?}", err);
						return Ok(());
					}
				};
				let Some(booru_posts) = booru_posts else {
					return Ok(())
				};
				for post in booru_posts {
					let hashtags = get_booru_post_tags(&post, None).await;
					let source = if domain == "danbooru.donmai.us" {
						format!("https://{domain}/posts/{}", post.id)
					} else {
						format!(
							"https://{}/index.php?page=post&s=view&id={}",
							domain, post.id
						)
					};
					let caption = format!(
						"{} — <a href=\"{source}\">sauce</a>",
						hashtags.join(" ").replace(
							[
								'\\', '!', '\'', ':', '{', '}', '+', '~', '(', ')', '.', ',', '/',
								'-'
							],
							""
						)
					);
					let button_data = match post.rating.as_str() {
						"safe" | "sensitive" | "general" | "s" | "g" => "sfw",
						"questionable" | "explicit" | "q" | "e" => "nsfw",
						_ => return Ok(()),
					};
					send_feed_post(&bot, msg.chat.id, post, &caption, button_data).await;
				}
			}

			Err(_) => (),
		}
	}

	Ok(())
}

async fn callback_handler(bot: Throttle<Bot>, q: CallbackQuery) -> Result<(), Box<dyn Error + Send + Sync>> {
	if let Some(data) = q.data {
		bot.answer_callback_query(q.id).await?;

		let Some(message) = q.message else {
			return Ok(())
		};

		if data == "delete" {
			bot.delete_message(message.chat.id, message.id).await?;
			return Ok(());
		}

		// retrocompatibility
		let mut data = data;
		if data == "safe" {
			data = "sfw".to_string();
		} else if data == "hentai" {
			data = "nsfw".to_string();
		}

		let users = read_users().unwrap();

		let Some(user) = users.iter().find(|&user| user.id == q.from.id.0) else {
			return Ok(())
		};

		let mut tags = vec![];
		let mut source = String::new();
		if let Some(entities) = message.parse_caption_entities() {
			let hashtags = entities
				.iter()
				.filter(|&e| e.kind() == &MessageEntityKind::Hashtag);
			for hashtag in hashtags {
				tags.push(hashtag.text());
			}
			for entity in entities {
				match entity.kind() {
					MessageEntityKind::TextLink { url } => {
						source = url.to_string();
						break;
					}
					// retrocompatibility
					MessageEntityKind::Url => {
						source = entity.text().to_string();
						break;
					}
					_ => (),
				}
			}
		}

		let mut chat: Option<UserChat> = None;
		for user_chat in &user.chats {
			if user_chat.rating == data {
				if let Some(whitelist) = &user_chat.whitelist {
					for whitelist_tag in whitelist {
						if tags.contains(&whitelist_tag.as_str()) {
							chat = Some(user_chat.clone());
							break;
						}
					}
				} else {
					chat = Some(user_chat.clone());
					break;
				}
			} else {
				continue;
			}
		}
		let Some(chat) = chat else {
			return Ok(())
		};

		let chat_id = ChatId(chat.id);

		let mut caption = chat.caption.clone();
		if let Some(link) = chat.link.clone() {
			caption = format!("{caption} {}", link);
		}

		if chat.has_tags {
			caption = format!(
				"{caption}\n{} — <a href=\"{source}\">sauce</a>",
				tags.join(" ")
			);
		}

		let sent_message_id: MessageId;
		if let Some(photos) = message.photo() {
			let photo = InputFile::file_id(photos.last().unwrap().file.id.clone());
			sent_message_id = bot
				.send_photo(chat_id, photo)
				.caption(caption)
				.parse_mode(Html)
				.await?
				.id;
			bot.delete_message(message.chat.id, message.id).await.ok();
		} else if let Some(video) = message.video() {
			let video = InputFile::file_id(video.file.id.clone());
			sent_message_id = bot
				.send_video(chat_id, video)
				.caption(caption)
				.parse_mode(Html)
				.await?
				.id;
			bot.delete_message(message.chat.id, message.id).await.ok();
		} else if let Some(animation) = message.animation() {
			let animation = InputFile::file_id(animation.file.id.clone());
			sent_message_id = bot
				.send_animation(chat_id, animation)
				.caption(caption)
				.parse_mode(Html)
				.await?
				.id;
			bot.delete_message(message.chat.id, message.id).await.ok();
		} else {
			return Ok(());
		}

		if let Some(queues) = &chat.queue {
			for queue_chat_id in queues {
				let path = format!("db/queue/{}", chat_id.0);
				std::fs::create_dir_all(path.clone()).unwrap();
				let mut queue_file = std::fs::File::options()
					.create(true)
					.append(true)
					.open(&format!("{path}/{queue_chat_id}"))
					.unwrap();
				writeln!(&mut queue_file, "{}", sent_message_id.0).unwrap();
			}
		}
		if let Some(forwards) = &chat.forward {
			for forward_chat_id in forwards {
				bot.forward_message(ChatId(*forward_chat_id), chat_id, sent_message_id)
					.await?;
			}
		}
	}

	Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	pretty_env_logger::init();
	log::info!("Starting buttons bot...");

	let api_url = match std::env::var("TELEGRAM_API_URL") {
		Ok(api_url) => {
			Some(url::Url::parse(&api_url).unwrap())
		}
		Err(_) => None,
	};

	let bot = match api_url {
		Some(api_url) => {
			Bot::from_env()
				.set_api_url(api_url)
				.throttle(Limits::default())
		}
		None => {
			Bot::from_env()
				.throttle(Limits::default())
		}
	};

	let handler = dptree::entry()
		.branch(Update::filter_message().endpoint(message_handler))
		.branch(Update::filter_callback_query().endpoint(callback_handler));

	match (std::env::var("HOST"), std::env::var("PORT")) {
		(Ok(host), Ok(port)) => {
			let port: u16 = port
				.parse()
				.expect("PORT env variable value is not an integer");
			let addr = ([0, 0, 0, 0], port).into();
			let url = format!("https://{host}").parse().unwrap();
			let listener = webhooks::axum(bot.clone(), webhooks::Options::new(addr, url))
				.await
				.expect("Couldn't setup webhook");
			let listener_eh = LoggingErrorHandler::new();
			Dispatcher::builder(bot, handler)
				.enable_ctrlc_handler()
				.build()
				.dispatch_with_listener(listener, listener_eh)
				.await;
		}
		_ => {
			Dispatcher::builder(bot, handler)
				.enable_ctrlc_handler()
				.build()
				.dispatch()
				.await;
		}
	}

	Ok(())
}
