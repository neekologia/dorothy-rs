use std::error::Error;
use std::fs::read_dir;
use std::io::{BufRead, BufReader, Write};
use std::thread::sleep;
use std::time::Duration;

use teloxide::requests::Requester;
use teloxide::types::{ChatId, MessageId};
use teloxide::Bot;

struct QueuedChat {
	from_chat_id: ChatId,
	to_chat_id: ChatId,
}

fn get_message_id(queue_path: &str, queued_chat: QueuedChat) -> Option<MessageId> {
	let path = format!(
		"{queue_path}/{}/{}",
		queued_chat.from_chat_id, queued_chat.to_chat_id
	);
	let mut queue_file = std::fs::File::options()
		.create(true)
		.write(true)
		.read(true)
		.open(&path)
		.unwrap();

	let queue_reader = BufReader::new(&queue_file);
	let mut message_id_iter = queue_reader.lines();

	if let Some(message_id) = message_id_iter.next() {
		let mut message_ids: Vec<String> = vec![];
		for id in message_id_iter {
			message_ids.push(id.unwrap());
		}
		write!(&mut queue_file, "{}", message_ids.join("\n")).unwrap();
		return Some(MessageId(message_id.unwrap().parse::<i32>().unwrap()));
	} else {
		return None;
	}
}

async fn queue_loop(bot: Bot) -> Result<(), Box<dyn Error>> {
	let queue_path = format!("db/queue/");
	loop {
		let Ok(from_chat_id_iter) = read_dir(&format!("{queue_path}")) else {
			sleep(Duration::from_secs(1200));
			continue;
		};
		for from_chat_id in from_chat_id_iter {
			let Ok(from_chat_id) = from_chat_id else {
				sleep(Duration::from_secs(5));
				continue;
			};

			if !from_chat_id.file_type().unwrap().is_dir() {
				sleep(Duration::from_secs(5));
				continue;
			}

			let Ok(to_chat_id_iter) = read_dir(&format!("{queue_path}/{}", from_chat_id.file_name().to_str().unwrap())) else {
				sleep(Duration::from_secs(5));
				continue;
			};
			for to_chat_id in to_chat_id_iter {
				let Ok(to_chat_id) = to_chat_id else {
					sleep(Duration::from_secs(5));
					continue;
				};

				if to_chat_id.file_type().unwrap().is_dir() {
					sleep(Duration::from_secs(5));
					continue;
				}

				let from_chat_id = ChatId(
					from_chat_id
						.file_name()
						.to_str()
						.unwrap()
						.parse::<i64>()
						.unwrap(),
				);
				let to_chat_id = ChatId(
					to_chat_id
						.file_name()
						.to_str()
						.unwrap()
						.parse::<i64>()
						.unwrap(),
				);
				let queued_chat = QueuedChat {
					from_chat_id,
					to_chat_id,
				};
				let Some(message_id) = get_message_id(&queue_path, queued_chat) else { continue };
				bot.forward_message(to_chat_id, from_chat_id, message_id)
					.await?;
			}
		}
		sleep(Duration::from_secs(7200))
	}
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	let bot = Bot::from_env();
	queue_loop(bot).await?;
	Ok(())
}
