use serde::Deserialize;
use teloxide::types::{
	InlineKeyboardMarkup,
	ChatId,
	InputFile,
	ParseMode,
};

#[derive(Deserialize, Debug)]
pub struct User {
	pub id: u64,
	pub name: String,
	pub chats: Vec<UserChat>,
}
#[derive(Deserialize, Debug, Clone)]
pub struct UserChat {
	pub rating: String,
	pub whitelist: Option<Vec<String>>,
	pub id: i64,
	pub name: String,
	pub caption: String,
	pub link: Option<String>,
	pub has_tags: bool,
	pub queue: Option<Vec<i64>>,
	pub forward: Option<Vec<i64>>,
}

#[derive(Deserialize, Debug)]
pub struct Booru {
	pub name: String,
	pub active: bool,
	pub amount: u64,
	pub rating: String,
	pub button: String,
	pub website: String,
	pub api_key: Option<String>,
	pub blacklist: Vec<String>,
	pub replace: Option<Vec<(String, String)>>,
	pub chats: Vec<BooruChat>,
}
#[derive(Deserialize, Debug)]
pub struct BooruChat {
	pub id: i64,
	pub tags: Vec<String>,
}

#[derive(Deserialize)]
pub struct GelbooruSource {
	pub post: Option<Vec<BooruPost>>,
}
#[derive(Deserialize, Debug, Clone)]
pub struct BooruPost {
	pub id: u64,
	pub file_url: String,
	pub sample_url: String,
	pub md5: Option<String>,
	pub hash: Option<String>,
	pub tags: String,
	pub rating: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct DanbooruPost {
	pub id: u64,
	pub rating: String,
	pub md5: String,
	pub tag_string: String,
	pub media_asset: DanbooruMediaAsset,
}
#[derive(Deserialize, Debug, Clone)]
pub struct DanbooruMediaAsset {
	pub variants: Vec<DanbooruVariant>,
}
#[derive(Deserialize, Debug, Clone)]
pub struct DanbooruVariant {
	pub r#type: String,
	pub url: String,
}

pub struct SendFeedPost {
	pub method: String,
	pub input_file: InputFile,
	pub chat_id: ChatId,
	pub caption: String,
	pub parse_mode: ParseMode,
	pub reply_markup: InlineKeyboardMarkup,
}
